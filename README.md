Skidmarks
=========
Skidmarks is a language independent lightweight code documentation tool.

Unlike other documentation tools, Skidmarks does not scan for keywords.
Instead all documentation sections are written in Markdown, which enable
very flexible and easy to customizable outputs. A 'side effect' is that
it automatically yields human readable documentation in the actual
source code.

## Usage

    $ luajit skidmarks.lua [options] <path_to_file | path_to_folder>

## Options

    Available options are:
      -h, --help                    Prints this help message.
      -v, --version                 Show skidmarks version.
      -o, --output  arg             Specify output directory where generated files will be stored.(default: output)
      --html        arg             Use custom HTML template file.
      -d, --dry-run                 No output will be saved.
      -i, --id      arg             Add a block symbol to identify comment blocks, i.e '///' for C-style source files

## Dependencies

* [LuaJIT 2.0](http://luajit.org/)
* [luafilesystem (lfs)](https://github.com/keplerproject/luafilesystem)
* [Sundown FFI](https://github.com/torch/sundown-ffi)
* [Sundown](https://github.com/vmg/sundown)

## TODO
* Fix: One invalid block is listed in index HTML.
* Fix: Better overview page in index HTML (make three sections: 'overview', 'files', 'blocks')
* Fix: Only create output directories that will contain output files.
* Add: Option to filter input filetypes.

## CHANGELOG
* Fix: Do not try to create folders that exists.
* Add: Generate index HTML for folder gather/compile.
* Fix: --output option should specify where the output will be saved.
* Add: Should be able to loop through directories.
* Fix: skidmarks should not have to include a space at the end
	to count as an empty row.