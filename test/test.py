"""
	skidmarks unit test for python source files
"""

### skidmarks_multiply
### ==================
### Performs multiplication of two numbers
### 
### params:
### * a - first number
### * b - second number
### 
### Returns the product of two numbers a and b
### 
### Code example: 
###  	print( "2 * 10 = " .. skidmarks_multiply(2,10) )
###     
###     => 2 * 10 = 20

def skidmarks_multiply(a,b):
	return a * b

### skidmarks_divide
### ================
### Performs division on two numbers
### 
### params:
### * a - first number
### * b - second number
### 
### Returns the quotient of two numbers a and b
### 
### Code example: 
###  	print( "10 / 2 = " .. skidmarks_divide(10,2) )
###     
###     => 10 / 2 = 5

def skidmarks_divide(a,b):
	return a / b

print( "2 * 10 = " + str(skidmarks_multiply(2,10)) )
print( "10 / 2 = " + str(skidmarks_divide(10,2)) )