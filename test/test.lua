--[[
	skidmarks unit test for Lua source files
]]

skidmarks = {}

--- skidmarks.multiply
--- ==================
--- Performs multiplication of two numbers
--- 
--- params:
--- * a - first number
--- * b - second number
--- 
--- Returns the product of two numbers a and b
--- 
--- Code example: 
---  	print( "2 * 10 = " .. skidmarks.multiply(2,10) )
---     
---     => 2 * 10 = 20
skidmarks.multiply = function(a,b)
	return a * b
end

--- skidmarks.divide
--- ================
--- Performs division on two numbers
--- 
--- params:
--- * a - first number
--- * b - second number
--- 
--- Returns the quotient of two numbers a and b
--- 
--- Code example: 
---  	print( "10 / 2 = " .. skidmarks.divide(10,2) )
---     
---     => 10 / 2 = 5
skidmarks.divide = function(a,b)
	return a / b
end

print( "2 * 10 = " .. skidmarks.multiply(2,10) )
print( "10 / 2 = " .. skidmarks.divide(10,2) )