/// Skidmarks unit test for C source files

#include <stdio.h>

/// skidmarks_multiply
/// ==================
/// Performs multiplication of two numbers
/// 
/// params:
/// * a - first number
/// * b - second number
/// 
/// Returns the product of two numbers a and b
/// 
/// Code example: 
/// 
///     print( "2 * 10 = " .. skidmarks_multiply(2,10) )
///     => 2 * 10 = 20
float skidmarks_multiply(float a, float b)
{
	return a * b;
}

/// skidmarks_divide
/// ================
/// Performs division on two numbers
/// 
/// params:
/// * a - first number
/// * b - second number
/// 
/// Returns the quotient of two numbers a and b
/// 
/// Code example: 
/// 
///  	print( "10 / 2 = " .. skidmarks_divide(10,2) )
///     => 10 / 2 = 5
float skidmarks_divide(float a, float b)
{
	return a / b;
}

int main(int argc, char const *argv[])
{
	printf("2 * 10 = %f\n", skidmarks_multiply(2.0f,10.0f));
	printf("10 / 2 = %f\n", skidmarks_divide(10.0f,2.0f));
	return 0;
}