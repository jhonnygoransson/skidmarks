package.path = "?.lua;" .. package.path
local sundown = require("sundown/init")
local lfs     = require("lfs")

---------------------------------------------------------------------
-- skidmarks code block identifiers
---------------------------------------------------------------------
skidmarks = { "---", 
			  "///", 
			  "###" }

---------------------------------------------------------------------
-- Application globals
---------------------------------------------------------------------
local version = "0.9.0"
local html_template = [[<!DOCTYPE html>
<html>
	<head>
		<title>{{title}}</title>
		
		<style type="text/css">
			body { font-family: sans-serif; }

			#header {
				
			}

			#toc {
				
			}

			#toc ul {
				
			}

			#content {
				border-top: 1px solid #ddd;
			}

			.block {
				margin: 20px;
			}

			#content code {
				
			}

			#overview,#blockunit {
				display : none;
			}

			#{{displaybody}} {
				display : block;
			}

		</style>

	</head>
	<body>
		<div id="overview">
			<div>Overview Header for {{overviewbase}}</div>

			<div id="overviewmethods">
				{{overviewmethods}}
			</div>

			<div id="overviewfiles">
				{{overviewfiles}}
			</div>
		</div>
		<div id="blockunit">
			<div id="header">
				{{header}}
			</div>
			<div id="header">
				{{description}}
			</div>
			<div id="toc">
				<h2>Jump to definition:</h2>
				{{toc}}
			</div>
			<div id="content">
				{{content}}
			</div>
		</div>
	</body>
</html>
]]

----------------------------------
-- Command line arg methods
----------------------------------
-- Pretty print argument lookup table
local pp_arguments = function ( arg_lut )
  for k,v in pairs(arg_lut) do
	
	local out_str = "  -" .. table.concat(v.options, ", -") .. "\t"

	if (v.sub_args and v.sub_args > 0) then
	  if (v.sub_args > 1) then

		local sa_list = {}
		for i=1,v.sub_args do
		  table.insert(sa_list, "arg" .. tostring(i))
		end
		out_str = out_str .. table.concat(sa_list, ", ")
	  else
		out_str = out_str .. "arg" .. "\t"
	  end
	else
	  out_str = out_str .. "\t"
	end

	if (v.description) then
	  out_str = out_str .. "\t" .. tostring(v.description)
	end

	if (v.default) then
	  out_str = out_str .. "(default: " .. tostring(v.default) .. ")"
	end

	print(out_str)

  end
end

----------------------------------
-- Parse arguments using argument lookup table
local parse_arguments = function ( arg_lut, arg_list )

	local ret_args   = {}
	local extra_args = {}
	local arg        = nil
	local sub_args   = {}
	local sub_count  = 0

	-- clear output list with default values
	for _,v in pairs(arg_lut) do
		ret_args[v.name] = v.default
	end

	-- parse all input args
	for i,v in ipairs(arg_list) do

		if (sub_count > 0) then
			table.insert(sub_args, v)
			sub_count = sub_count - 1

			if (sub_count == 0) then
				if (type(arg.value) == "function" ) then
					ret_args[arg.name] = arg.value(sub_args)
				else
					if (#sub_args == 1) then
						sub_args = sub_args[1]
					end
					ret_args[arg.name] = sub_args --arg.value
				end
			end
		else

			-- find correct option, loop argument lut
			local found_opt = false
			if (v:sub(1,1) == "-") then

				for _,a in pairs(arg_lut) do

					-- loop option variations
					for _,o in pairs(a.options) do
						
						if ("-" .. o == v) then
							arg	 = a
							found_opt = true
							break
						end

					end

					if (found_opt) then
						break
					end
				end

				-- see if this is an unknown argument
				if (not found_opt) then
					print("Unknown argument!")
					os.exit(-1)
				end

				-- should we collect sub_arguments?
				sub_args = {}
				if (arg.sub_args and arg.sub_args > 0) then
					sub_count = arg.sub_args
				else
					if (type(arg.value) == "function" ) then
						ret_args[arg.name] = arg.value()
					else
						ret_args[arg.name] = true
					end
				end

			else
				--print("cool arg: " .. v)
				table.insert(extra_args, v)
			end
		end
	end

	return ret_args, extra_args
end

-- sanity clean a path
function _aux_clean_path( path )

	path = string.gsub(path, '(\\)', "/") -- convert to "unix" style, don't worry windows can handle this
	path = string.gsub(path, '(/+)', "/") -- remove redundant separators

	return path
end

function _aux_isdir( path )
	local f,e = io.open("docs/", "r")
	if (f) then
		f:close()
		return true
	end

	return false
end

-----------------------------------------
-- loops through recursively the supplied directory path and
-- stores a tree of all the encountered files
function build_tree( path, options )
	local tree = {}

	local rel_len 	= #path + 2
	local dir_stack = { path }

	-- is file?
	local attr = lfs.attributes(path)
	if ( attr and attr.mode == "file" ) then	 	
		table.insert(tree, {path, path, path} )
	else
		-- loop through directory stack
		for _,dir in pairs(dir_stack) do
			-- loop every file entry in current directory
			for file in lfs.dir( dir ) do

				-- try not to get lost..
				if file ~= "." and file ~= ".." then

					local f = dir .. '/' .. file
					local rel_path = string.sub(f, rel_len )
					f = _aux_clean_path( f )
					local attr = lfs.attributes(f)
					
					if attr and attr.mode == "directory" then
						
							-- create ouput directory for this relative directory
							if (not _aux_isdir( options.output .. rel_path ) ) then
								local s,e = lfs.mkdir( options.output .. rel_path )
								if (not s) then
									print("Could not create output directory: " .. options.output .. rel_path)
									print("Error: " .. tostring(e) )
									os.exit(-1)
								end
							end

							table.insert(dir_stack, f)
						
					else

						table.insert( tree, { f, rel_path, file } )
					end
				end
			end
		end
	end

	return tree
end

function gather_and_run( tree_node, options )

	local path     = tree_node[1]
	local relpath  = tree_node[2]
	local filename = tree_node[3]

	-- construct output filepath
	local htmlpath 	= options.output .. relpath .. ".html"

	-- collect markdown-blocks
	local blocks 	= extract_markdown( path )

	-- "render" blocks and output to file
	if blocks and markdown_to_html( relpath, blocks, htmlpath, options ) then end
	return blocks,relpath
end

extract_markdown = function( path )
	io.write("  Extracting from " .. tostring(path) )

	local blocks = {}
	local f,e   = io.open(path)

	if e ~= nil then
		print("    Extract_markdown Failed, reason - [" .. e .. "]")
		return
	end

	--
	-- santas little helper methods
	--
	local startswith = function(s,pattern)
		-- http://lua-users.org/wiki/StringRecipes
		return string.sub( s,1,string.len(pattern))==pattern
	end

	local endswith = function(s,pattern)
		return string.sub( s, #s-#pattern+1 ) == pattern
	end

	local stripidentifier = function(s,pattern)
		return string.sub( s, #pattern+1)
	end

	local stripprefixspace = function(s)
		return string.match(s,"%s(.+)") or s
	end

	local tablelen = function(tbl)
		local s = 0
		for k,v in pairs(tbl) do 
			s = s + 1
		end
		return s 
	end

	-- append space to skidmarks
	for k,v in pairs(skidmarks) do 
		if ( not endswith(v," ") ) then
			skidmarks[k] = v .. " "
		end
	end

	local linecount = 0

	-- iterate lines 
	while true do 
		local line      = f:read("*line")
		linecount       = linecount + 1
		local linestart = -1
		local pattern   = nil
		local descblock = false

		if not line then break end

		for k,v in pairs( skidmarks ) do 
			if ( startswith(line,v) ) then
				pattern   = v
				linestart = linecount
				break
			end	
		end

		-- found a match, get all lines until end of blcok
		if pattern ~= nil then 
			descblock = linestart == 1

			-- strip whitespace from first line and use it as buffer name
			local buffer     = {}
			local buffername = stripidentifier(line,pattern)
				  buffername = string.match(buffername,"[^%s]+")

			while( line and startswith(line,pattern) ) do
				-- remove pattern identifier
				local strippedline = stripidentifier(line,pattern)
					  -- strippedline = stripprefixspace(strippedline)

				table.insert(buffer,strippedline)
				line      = f:read("*line")
				linecount = linecount + 1
			end

			table.insert( blocks, { buffername, linestart, table.concat(buffer,"\n"), descblock } )

		end
	end

	io.write(" ... extracted " .. tablelen(blocks) .. " blocks\n")

	f:close()

	return tablelen(blocks) > 0 and blocks or nil
end

markdown_to_html = function( file_name, blocks, outpath, options )
	print("  Converting Markdown src to HTML")

	local htmlbuffer      = {}
	local full_html       = html_template
	local toc_buffer      = {}
	local toc_description = "No description available."

	-- render markdown
	for k,v in pairs(blocks) do 
		local blockname   = v[1]
		local linestart   = v[2]
		local blockbuf    = v[3]
		local description = v[4]

		local block_id = blockname -- TODO clean up block id to it can be used as a hash anchor

		if ( not description ) then
			table.insert( toc_buffer, { name = blockname, id = block_id, line = linestart } )

			local html = "<div class='block'><a id='" .. block_id .. "'></a>"
				  html = html .. sundown.render(blockbuf) .. "</div>"
			table.insert(htmlbuffer,html)
		else
			toc_description = sundown.render(blockbuf)
		end
	end

	local blocks_html = table.concat(htmlbuffer)

	-- generate a toc
	local toc_html = '<ul>'
	for k,v in pairs(toc_buffer) do
		toc_html = toc_html .. '<li><a href="#' .. v.id .. '">[' .. v.line .. '] : ' .. v.name .. '</a></li>'
	end
	toc_html = toc_html .. "</ul>"
	
	-- replace template-tags with real content
	full_html = string.gsub(full_html, "{{(%w+)}}",
		{ title       = file_name,
		  toc         = toc_html,
		  header      = "<h1>" .. file_name .. "</h1>",
		  description = toc_description,
		  content     = blocks_html,
		  displaybody = "blockunit"
		})

	if (options.dry_run) then
		return
	end

	print("  Writing HTML to " .. outpath)

	local f,e = io.open(outpath,"w")
	if e ~= nil then
		print("    Failed, reason - [" .. e .. "]")
		return
	end

	f:write(full_html)
	f:close()
end

---------------------------------------------------------------------
-- skidmarks_cli main entry
---------------------------------------------------------------------
local skidmarks_cli = function (  )
	
	local arg_lut = {
	{ name = "help",
	  options = { "h", "-help" },
	  description = "Prints this help message." },

	{ name = "version",
	  options = { "v", "-version" },
	  description = "Show skidmarks version." },

	{ name = "output",
	  options = { "o", "-output" },
	  default = "docs/",
	  sub_args = 1,
	  description = "Specify output directory where generated files will be stored." },

	{ name = "template",
	  options = { "t", "-template" },
	  sub_args = 1,
	  description = "Use custom HTML template file." },

	{ name = "dry_run",
	  options = { "d", "-dry-run" },
	  description = "No output will be saved." },

	{ name = "identifier",
	  options = {"i", "-id"},
	  sub_args = 1,
	  description = "Add a block symbol to identify comment blocks, i.e '///' for C-style source files" }
	}
	local arg_options, extra_args = parse_arguments( arg_lut, arg )

	-- show usage help and arguments
	if (arg_options.help or #arg <= 0 ) then
		print("usage: luajit skidmarks.lua [options] <path_to_file | path_to_folder>")
		print("Available options are:")
		pp_arguments(arg_lut)
		os.exit(1)
	end

	-- show version 
	if (arg_options.version) then
		print("skidmarks " .. tostring(version))
		os.exit(1)
	end

	-- change HTML template if custom HTML path was supplied
	if (arg_options.template) then
		local f,e = io.open(arg_options.template, "r")
		if (not f) then
			print("Could not read HTML template: " .. tostring(e))
			os.exit(-1)
		end
		html_template = f:read("*a")
		f:close()
	end

	if (arg_options.identifier) then
		table.insert(skidmarks, arg_options.identifier)
	end

	-- cleanup output path string
	arg_options.output = arg_options.output .. "/"
	arg_options.output = _aux_clean_path( arg_options.output )

	-- create output directory
	if (not _aux_isdir( arg_options.output )) then
		local s,e = lfs.mkdir( arg_options.output )
		if (not s) then
			print("Could not create output directory: " .. arg_options.output )
			print("Error: " .. tostring(e) )
			os.exit(-1)
		end
	end

	-- build file tree
	local tree = build_tree( extra_args[1], arg_options )

	-- compile file tree
	local allblocks = {}
	for k,v in pairs(tree) do 
		local blocks,path   = gather_and_run( v, arg_options )

		if ( blocks ) then 
			table.insert( allblocks, { blocks, path } )
		end
	end

	-- generate overview
	local overviewindexpath = arg_options.output .. "index.html"
	local overviewindexhtml = html_template
	local overviewmethods   = {}
	local overviewfiles 	= {}

	for _,blocktbl in pairs(allblocks) do
		local block     = blocktbl[1]
		local blockpath = blocktbl[2]

		table.insert(overviewfiles, "<li><a href='" .. blockpath .. ".html'>" .. blockpath .. "</a>")

		for k,v in pairs(block) do 
			local blockname   = v[1]
		local linestart   = v[2]
		local blockbuf    = v[3]
		local description = v[4]

		table.insert(overviewmethods,"<li><a href=''>" .. blockname .. "</a></li>")
		end
	end

	local overviewmethodshtml = "<list><ol>" .. table.concat(overviewmethods, "") .. "</ol></list>"
	local overviewfileshtml = "<list><ol>" .. table.concat(overviewfiles,"") .. "</ol></list>"


	overviewindexhtml = string.gsub(overviewindexhtml, "{{(%w+)}}",
		{ 
			displaybody 		= "overview",
			overviewmethods 	= overviewmethodshtml,
			overviewbase 		= extra_args[1],
			overviewfiles  	= overviewfileshtml
		})

	if ( not arg_options.dry_run ) then
		local overviewfile,e    = io.open(overviewindexpath,"w")
		overviewfile:write(overviewindexhtml)
		overviewfile:close()
	end
end

skidmarks_cli()

